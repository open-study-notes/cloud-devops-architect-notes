# AWS SSA-03 Study Notes

First, let's set some expectations of this notes:

- We’re going to prepare for the Solutions Architect exam - SAA-C03
- It’s a challenging certification, so this course will be long and interesting
- Basic IT knowledge is necessary
- From the Cloud Practitioner, Developer and SysOps course - shared knowledge
- Specific to the Solutions Architect exam - exciting ones on architecture!
- This will cover over 30 AWS services

---

[[_TOC_]]

## IAM & AWS CLI

- IAM = Identity and Access Management, *Global service*
- *Root account* created by default, shouldn’t be used or shared
- Users are people within your organization, and can be grouped
- Groups only contain users, not other groups
- Users don’t have to belong to a group, and user can belong to multiple groups

<img src="image/iamgroups.png" width="500">

### IAM Permissions

Best practice is to create individual IAM account for each user and assign them into corresponding groups and manage the permission at group level. Never share/use root account unless necessary.

- Users or Groups can be assigned JSON documents called policies
- These policies define the permissions of the users
- In AWS you apply the least privilege principle: don’t give more permissions than a user needs

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "ec2:Describe*",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "elasticloadbalancing:Describe*",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "cloudwatch:ListMetrics",
                "cloudwatch:GetMetricStatistics",
                "cloudwatch:Describe*"
            ],
            "Resource": "*"
        }
    ]
}

```

### IAM Policies Structure

The IAM Policy consists of:

- **Version**: policy language version, always include “2012-10-17”
- **Id**: an identifier for the policy (optional)
- **Statement**: one or more individual statements (required)
  
The Statements consists of:

- **Sid**: an identifier for the statement (optional)
- **Effect**: whether the statement allows or denies access (Allow, Deny)
- **Principal**: account/user/role to which this policy applied to
- **Action**: list of actions this policy allows or denies
- **Resource**: list of resources to which the actions applied to
- **Condition**: conditions for when this policy is in effect (optional)

```json
{
    "Version": "2012-10-17",
    "Id": "S3-Account-Permissions",
    "Statement": [
        {
            "Sid": "1",
            "Effect": "Allow",
            "Principal": {
                "AWS": ["arn:aws:iam::123123123123:root"]
            },
            "Action": ["s3:getObject","s3:PutObject"],
            "Resource": ["arn:aws:s3:::mybucket/*"]
        }
    ]
}

```

### IAM Password Policy

There are some password policy we can use:

1. Set a minimum password length
2. Require specific character types
3. Allow all IAM users to change their own passwords
4. Set password expiration period
5. Prevent password re-use

**"Multi Factor Authentication - MFA"** is the most recommended way to protect all IAM user accounts. Therefore even if the password was compromised, the account is still bing protected by the MFA device.

### AWS Access Keys, CLI and SDK

To access AWS, you have three options:

- AWS Management Console (protected by password + MFA)
- AWS Command Line Interface (CLI): protected by access keys
- AWS Software Developer Kit (SDK) - for code: protected by access keys
  
**Access Keys are secret, just like a password. Don’t share them**

- Access Key ID ~ username
- Secret Access Key ~ password

**AWS CLI** enables you to interact with AWS services using commands in your own Command-line shell. Source Code [GitHub page](https://github.com/aws/aws-cli).

**AWS SDK** is the AWS Software Development Kit (AWS SDK) that provides Language-specific APIs (set of libraries). It also enables you to access and manage AWS services programmatically. you can embed it within your application Supports for:

- Mobile SDKs (Android, iOS, …)
- IoT Device SDKs (Embedded C, Arduino, …)
- Example: AWS CLI is built on AWS SDK for Python

### IAM Roles for Services

Some AWS service will need to perform actions on behalf of you, thus we will assign **permissions** to AWS services with **IAM Roles**. Common roles are:
 1. EC2 Instance Roles
 2. Lambda Function Roles
 3. Roles for CloudFormation

### IAM Security Tools

There are two main tools we can use to enforce the least privilege principle:

- **IAM Credentials Report (account-level)**: a report that lists all your account's users and the status of their various credentials
- **IAM Access Advisor (user-level):** Access advisor shows the service permissions granted to a user and when those services were last accessed

### IAM Guidelines & Best Practices

- Don’t use the root account except for AWS account setup
- One physical user = One AWS user
- Assign users to groups and assign permissions to groups
- Create a strong password policy
- Use and enforce the use of Multi Factor Authentication (MFA)
- Create and use Roles for giving permissions to AWS services
- Use Access Keys for Programmatic Access (CLI / SDK)
- Audit permissions of your account using IAM Credentials Report & IAM Access Advisor
- Never share IAM users & Access Keys

### IAM Section Summary

- **Users:** mapped to a physical user, has a password for AWS Console
- **Groups:** contains users only 
- **Policies:** JSON document that outlines permissions for users or groups
- **Roles:** for EC2 instances or AWS services
- **Security:** MFA + Password Policy
- **AWS CLI:** manage your AWS services using the command-line
- **AWS SDK:** manage your AWS services using a programming language
- **Access Keys:** access AWS using the CLI or SDK
- **Audit:** IAM Credential Reports & IAM Access Advisor

## EC2 Fundamentals 

### EC2 Basics

EC2 is one of the most popular and basic offering from AWS. EC2 = Elastic Compute Cloud = IaaS
The main capability includes:

 1. Renting virtual machines (EC2)
 2. Storing data on virtual drives (EBS)
 3. Distributing load across machines (ELB)
 4. Scaling the services using an auto-scaling group (ASG)

 It comes with different sizing & configuration options. 
 
 ## EC2 - SAA Level

 ### Private vs Public vs Elastic IP

 We will skip the private & public IP part and only foucs on Elastic IP
 Key points about Elastic IPs:

- When you stop and then start an EC2 instance, it can change its public IP
- If you need to have a fixed public IP for your instance, you need an Elastic IP
- An Elastic IP is a public IPv4 IP you own as long as you don’t delete it
- You can attach it to one instance at a time
- With an Elastic IP address, you can mask the failure of an instance or software by rapidly remapping the address to another instance in your account. 
- You can only have 5 Elastic IP in your account (you can ask AWS to increase that)

Overall, try to avoid using Elastic IP. They often reflect poor architectural decisions, and we should use a random public IP and register a DNS name to it. Or, as we’ll see later, use a Load Balancer and don’t use a public IP.

### Placement Groups

**Cluster**: clusters instances into a low-latency group in a single Availability Zone
<img src="image/cluster_placement.png" width="500">

**Spread**: spreads instances across underlying hardware (max 7 instances per 
group per AZ)
<img src="image/spread_placement.png" width="500">

**Partition**: spreads instances across many different partitions (which rely on different sets of racks) within an AZ. Scales to 100s of EC2 instances per group 
(Hadoop, Cassandra, Kafka)
<img src="image/partition_placement.png" width="500">